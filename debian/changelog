python-trezor (0.13.10-2) unstable; urgency=medium

  * Remove unneeded debian/source/options with extend-diff-ignore.
  * debian/control:
    - Add build-depends-indep on python3-shamir-mnemonic and python3-slip10.
    - Bump standards-version to 4.7.2 (no changes needed).

 -- Soren Stoutner <soren@debian.org>  Mon, 03 Mar 2025 15:09:13 -0700

python-trezor (0.13.10-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Relax python3-click upper bounds (Closes: #1098252).
    - Move python3-cryptography from recommends to build-depends-indep to match
      changes in upstream.
    - Move most build-depends to build-depends-indep.
    - Bump standards-version to 4.7.1 (no changes needed).
  * debian/copyright:
    - Update copyright year and email address for myself.
  * debian/gbp.conf:  Enforce pristine-tar and sign-tags.
  * debian/salsa-ci.yml:  Update with the currently recommended default
    contents.

 -- Soren Stoutner <soren@debian.org>  Tue, 22 Oct 2024 09:49:29 -0700

python-trezor (0.13.9-4) unstable; urgency=medium

  * debian/rules:  Improve test override comment.

 -- Soren Stoutner <soren@debian.org>  Mon, 07 Oct 2024 21:32:33 -0700

python-trezor (0.13.9-3) unstable; urgency=medium

  * debian/control:  Add comments explaining recommended and suggested packages.

 -- Soren Stoutner <soren@debian.org>  Mon, 07 Oct 2024 13:26:14 -0700

python-trezor (0.13.9-2) unstable; urgency=medium

  * Add debian/README.Debian to document recommended and suggested packages and
    unpackaged components.
  * Add debian/python3-trezor.docs to install upstream README.md and
    debian/README.Debian.
  * Add debian/trezor.docs to install upstream README.md.
  * debian/control:
    - Remove unneeded Build-Depends and Recommends on python3-attr.
    - Update Build-Depends from python3-libusb1 (transitional package) to
      python3-usb1.
    - Recommend python3-cryptography and python3-pyqt5.
    - Move python3-hid from Recommends to Suggests.
    - Suggest cython3, libudev-dev, libusb-1.0-0-dev, and python3-dev.
  * debian/io.trezor.trezorctl.metainfo.xml:  Update homepage and bugtracker
    ULRs.

 -- Soren Stoutner <soren@debian.org>  Fri, 04 Oct 2024 11:19:55 -0700

python-trezor (0.13.9-1) unstable; urgency=medium

  * New upstream release (Closes: #1062442, #1082274).
  * Remove unneeded lintian-overrides.
  * Remove debian/tests as everything is handled by
  * Add:
    - debian/trezorctl.1.
    - debian/trezor.manpages.
  * debian/control:
    - Add myself to uploaders.
    - Remove Richard Ulrich <richi@paraeasy.ch> from uploaders at his request.
    - Update upstream homepage.
    - Add Build-Depends on python3-construct-classes.
    - Add Testsuite: autopkgtest-pkg-pybuild.
    - Add <!nocheck> to python3-pytest.
    - Bump standards version to 4.7.0 (no changes needed).
    - Reformat for readability.
  * debian/copyright:
    - Add myself to debian/*.
    - Update upstream copyright.
    - Update upstream homepage.
    - Update license for debian/io.trezor.trezorctl.metainfo.xml to be Expat.
    - Reformat for readability.
  * debian/io.trezor.trezorctl.metainfo.xml:
    - Change license from MIT-0 to MIT (Closes: #1077138).
    - Refactor developer name.
  * debian/patches:
    - Refresh 0001-Suggest-apt-instead-of-pip-install.patch and update headers.
    - Remove 0002-fix-python-click-8.1-compatibility.patch (fixed
      upstream).
  * debian/rules:
    - Disable the new test_firmware during build as it accesses the internet.
  * debian/upstream/metadata:  Update Repository-Browse.
  * debian/watch:  Bump version from 3 to 4.

 -- Soren Stoutner <soren@debian.org>  Mon, 30 Sep 2024 10:41:04 -0700

python-trezor (0.12.4-3) unstable; urgency=medium

  * Team upload
  * Remove retired uploader

 -- Bastian Germann <bage@debian.org>  Thu, 13 Jul 2023 15:00:27 +0200

python-trezor (0.12.4-2) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Sebastian Ramacher ]
  * debian/patches: Backport upstream fix for click 8.1 (Closes: #1032731)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 25 Mar 2023 21:49:45 +0100

python-trezor (0.12.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * New upstream release; Closes: #997820
  * debian/copyright
    - update for new upstream code

 -- Sandro Tosi <morph@debian.org>  Sat, 30 Oct 2021 21:39:20 -0400

python-trezor (0.12.2-2) unstable; urgency=medium

  * Suggest apt instead of pip install.
  * Add missing Recommends: python3-attr.

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 29 Aug 2020 19:53:27 +0200

python-trezor (0.12.2-1) unstable; urgency=medium

  * New upstream release.
  * Update upstream URLs.

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 29 Aug 2020 13:48:29 +0200

python-trezor (0.12.1-4) unstable; urgency=medium

  * Add Recommends: python3-hid for supporting older firmware.

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 22 Aug 2020 21:56:10 +0200

python-trezor (0.12.1-3) unstable; urgency=medium

  * Expand py3dist-overrides for more precise dependencies.

 -- Tristan Seligmann <mithrandi@debian.org>  Fri, 21 Aug 2020 19:46:30 +0200

python-trezor (0.12.1-2) unstable; urgency=medium

  * Tighten trezor dependency on python3-trezor.

 -- Tristan Seligmann <mithrandi@debian.org>  Fri, 21 Aug 2020 18:02:37 +0200

python-trezor (0.12.1-1) unstable; urgency=medium

  * New upstream release.
  * Run real tests from autopkgtest.
  * Set upstream metadata fields: Repository, Repository-Browse.

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 10 Aug 2020 19:37:26 +0200

python-trezor (0.12.0-1) unstable; urgency=medium

  [ Tristan Seligmann ]
  * Set Maintainer to DPMT.

  [ Emmanuel Arias ]
  * New upstream version 0.12.0 (Closes: #947503).
  * d/io.trezor.trezorctl.metainfo.xml: Fix metadata license.
    - Change wrong mit license to gpl-3+.
  * d/salsa-ci.yml: enable salsa-ci.
  * d/gbp.conf: use debian/master branch as default.
  * d/tests: add autopkgtest-pkg-python.conf to add correct
    import-name.

  [ Tristan Seligmann ]
  * Bump Standards-Version to 4.5.0 (no changes).
  * Bump debhelper-compat to 13.
  * Declare rootless build.
  * Switch to dh-sequence-*.
  * Pull in pytest to run the test suite.
  * Rename Lintian tag.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 02 Aug 2020 11:47:19 +0200

python-trezor (0.11.6-1) unstable; urgency=medium

  [Emmanuel Arias]
  * New upstream release
  * Add me on d/copyright file
  * Remove Python 2 support
  * d/control: Bump debhelper-compat to 12
  * d/control: Bump Standar-Version to 4.4.1
  * d/copyright: Add me to debian/* files field.

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Tristan Seligmann ]
  * New upstream release.
  * Fix deps.

 -- Tristan Seligmann <mithrandi@debian.org>  Wed, 29 Jan 2020 14:11:36 +0200

python-trezor (0.9.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Tristan Seligmann ]
  * New upstream release.
  * Bump Standards-Version to 4.1.3 (no changes).
  * Fix Depends/Breaks/Replaces to handle move of trezorctl from
    python-trezor to trezor (closes: #884417).
  * Bump debhelper comat level to 11.

 -- Tristan Seligmann <mithrandi@debian.org>  Sat, 10 Mar 2018 19:16:36 +0200

python-trezor (0.7.16-3) unstable; urgency=medium

  * Fix Build-Depends on python{,3}-hidapi; these should be python{,3}-hid
    (closes: #885105).
  * Add py{,3}dist-overrides for hidapi.
  * Ship AppStream metadata with modalias provides, so this package will
    be suggested upon attaching a TREZOR.
  * Install bash completion snippet.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 24 Dec 2017 13:46:32 +0200

python-trezor (0.7.16-2) unstable; urgency=medium

  * Add some missing copyright information (closes: #884188).

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 17 Dec 2017 11:43:20 +0200

python-trezor (0.7.16-1) unstable; urgency=medium

  * New upstream release.
  * Build Python 3 package and switch to pybuild.
  * Update udev rules.
  * Bump Standards-Version to 4.1.2 (no changes).
  * Use https for pypi.debian.net.

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 11 Dec 2017 07:48:42 +0200

python-trezor (0.7.6-1) unstable; urgency=medium

  * new upstream release without changelog

 -- Richard Ulrich <richi@paraeasy.ch>  Thu, 20 Oct 2016 22:05:16 +0200

python-trezor (0.6.13-1) unstable; urgency=medium

  * new upstream release without changelog

 -- Richard Ulrich <richi@paraeasy.ch>  Thu, 04 Aug 2016 21:50:43 +0200

python-trezor (0.6.11-1) unstable; urgency=medium

  [ Richard Ulrich ]
  * new upstream release without changelog
  * updated udev rules

  [ Tristan Seligmann ]
  * Bump Standards-Version to 3.9.8 (no changes).
  * Update Vcs-* URLs.
  * Add pydist-overrides to get versioned dependencies.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 29 May 2016 15:23:31 +0200

python-trezor (0.6.10-1) unstable; urgency=medium

  * new upstream release without changelog

 -- Richard Ulrich <richi@paraeasy.ch>  Thu, 14 Jan 2016 22:32:13 +0100

python-trezor (0.6.5-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git
  * Initial release. (Closes: #763376)

 -- Richard Ulrich <richi@paraeasy.ch>  Wed, 24 Sep 2014 22:57:50 +0200
