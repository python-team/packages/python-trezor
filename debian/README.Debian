RECOMMENDED PACKAGES
--------------------

python3-cryptography:

Provides user-friendly device authentication for Trezor Safe 3 and newer models
with the `trezorctl device authenticate` command.


python3-pil and python3-pyqt5:

Trezor recommends python3-pil (also known as pillow or the Python Imaging
Library) and python3-pyqt5 (the Python 3 Qt 5 bindings), which facilitate
entering the PIN through a Qt widget on a computer screen.


SUGGESTED PACKAGES
------------------

cython3, libudev-dev, libusb-1.0-0-dev, pytonn3-dev, python3-hid:

If you have a Trezor One with firmware older than 1.7.0 and bootloader older
than 1.6.0 (including pre-2021 fresh-out-of-the-box units), they will not be
recognized by Trezor.  However, if you install cython3, libudev-dev,
libusb-1.0-0-dev, pytonn3-dev, and python3-hid, you can update the firmware to a
modern version.


UNPACKAGED COMPONENTS
---------------------

web3:

Ethereum signing from command line requires web3, which isn't currently packaged
in Debian.

https://pypi.org/project/web3/

You might be able to enable this functionality by running the following command:

pip install web3

Note that Debian cannot vouch for the security of what the above command
installs.  You should do your own due diligence before running it.


stellar-sdk:

Stellar signing from command line requires stellar-sdk, which isn't currently
packaged in Debian.

https://pypi.org/project/stellar-sdk/

You might be able to enable this functionality by running the following command:

pip install stellar-sdk

Note that Debian cannot vouch for the security of what the above command
installs.  You should do your own due diligence before running it.


 -- Soren Stoutner <soren@debian.org>  Fri, 04 Oct 2024 11:13:33 -0700
