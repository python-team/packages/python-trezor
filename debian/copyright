Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-trezor
Source: https://github.com/trezor/trezor-firmware/tree/main/python

Files:     *
Copyright: 2012-2022 SatoshiLabs and contributors
License:   LGPL-3

Files: debian/*
Copyright: 2014-2015 Richard Ulrich <richi@paraeasy.ch>
           2019 Emmanuel Arias <emmnuelarias30@gmail.com>
           2024-2025 Soren Stoutner <soren@debian.org>
License: GPL-3+
Comment:
 Soren Stoutner also optionally licenses his contributions under the LGPL-3
 license so they can be incorporated upstream in needed.

Files: debian/io.trezor.trezorctl.metainfo.xml
Copyright: 2018 Tristan Seligmann <mithrandi@debian.org>
           2024 Soren Stoutner <soren@debian.org>
License: Expat
Comment:
 Only a few licenses are allowed for AppStream Metadata, one of them being Expat
 (MIT).

Files: debian/trezor.manpages
       debian/trezorctl.1
Copyright: 2024 Soren Stoutner <soren@debian.org>
License: GPL-3+ or LGPL-3
Comment:
 Soren Stoutner licenses his contributions under either the GPL-3+ or the LGPL-3
 licenses so that they can be incorporated upstream if needed.  These are files
 where he is the sole author.

License: LGPL-3
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License version 3 as published by
 the Free Software Foundation.
Comment:
 On Debian systems, the full text of the GNU Lesser General Public License
 version 3 can be found in the file '/usr/share/common-licenses/LGPL-3'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, either version 3 of the License, or (at your option) any later
 version.
Comment:
 On Debian systems the full text of the GNU General Public License version 3 is
 located in '/usr/share/common-licenses/GPL-3'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
